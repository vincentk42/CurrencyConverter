/*
Note to self make sure to notice that the const that we are exporting is returing something inside
a parens() ..not a curly bracket{}!

*/

import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import moment from 'moment';

import styles from './styles';

const LastConverted = ({date, base, conversionRate, quote}) => (
    <Text style={styles.smallText}>
    1{base} = {conversionRate} {quote} as of {moment(date).format("MMMM D, YYYY")}

    </Text>
);


LastConverted.propTypes = {
    date: PropTypes.object,
    base: PropTypes.string,
    quote:PropTypes.string,
    conversionRate: PropTypes.number, 
    
}
export default LastConverted;