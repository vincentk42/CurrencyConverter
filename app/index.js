import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Home from './screens/Home'

EStyleSheet.build({
    $primaryBlue: '#4F6D7A',
    $white: '#FFFFFF',
    $border:'#E2E2E2',
    $inputText:'#797979',
    $lightGray:'#a9a9a9',
    //$outline:1,
});

export default () =>  <Home />;

/*
NOTE TO SELF you must put the $ in front of any variable in order make sure it's globa.,
simply using outline will result in an error
*/

